AggregatedAppList
=====

# Apps

## System
[] - remote access - https://guacamole.apache.org/

## Personal
[] - fitness assistant - https://github.com/wger-project/wger
[] - habit tracker - https://github.com/HabitRPG/habitica
[] - coder - https://www.youtube.com/watch?v=wtymUZQuir4
[] - psycopaths database - https://github.com/monicahq/monica

## Public
[] - private TV station - https://ersatztv.org/user-guide/install/
[] - irc web client - https://github.com/thelounge/thelounge-docker
[] - discord alternative - https://github.com/revoltchat

## ECommerce
[] - https://ofbiz.apache.org/
[] - https://www.opencart.com/
[] - https://github.com/spree/spree - https://hub.docker.com/r/spreecommerce/spree
[] - https://www.konakart.com/downloads/community_edition/
[] - https://www.zen-cart.com/

## Business Management


# Other Lists
https://github.com/awesome-selfhosted/awesome-selfhosted#personal-dashboards

# OSs
[] - QubeOS - https://www.qubes-os.org/